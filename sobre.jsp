<%--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Home page JSP
  -
  - Attributes:
  -    communities - Community[] all communities in DSpace
  -    recent.submissions - RecetSubmissions
  --%>

<%@page import="org.dspace.core.Utils"%>
<%@page import="org.dspace.content.Bitstream"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="java.io.File" %>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Locale"%>
<%@ page import="javax.servlet.jsp.jstl.core.*" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.app.webui.components.RecentSubmissions" %>
<%@ page import="org.dspace.content.Community" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.NewsManager" %>
<%@ page import="org.dspace.browse.ItemCounter" %>
<%@ page import="org.dspace.content.Metadatum" %>
<%@ page import="org.dspace.content.Item" %>

<%
    Community[] communities = (Community[]) request.getAttribute("communities");

    Locale sessionLocale = UIUtil.getSessionLocale(request);
    Config.set(request.getSession(), Config.FMT_LOCALE, sessionLocale);
    String topNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-top.html"));
    String sideNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-side.html"));

    boolean feedEnabled = ConfigurationManager.getBooleanProperty("webui.feed.enable");
    String feedData = "NONE";
    if (feedEnabled)
    {
        feedData = "ALL:" + ConfigurationManager.getProperty("webui.feed.formats");
    }
    
    ItemCounter ic = new ItemCounter(UIUtil.obtainContext(request));

    RecentSubmissions submissions = (RecentSubmissions) request.getAttribute("recent.submissions");
%>

<dspace:layout locbar="nolink" titlekey="jsp.home.title" feedData="<%= feedData %>">

	<!--div class="jumbotron">
        <%= topNews %>
	</div-->

<div class="container row">
	
	<div class="col-md-12">
	
		<h3>Sobre</h3>
		
		O Repositório Digital Institucional da <strong>Faculdade Pernambucana de Saúde</strong> visa permitir acesso livre, a produção acadêmica, científica, técnica e cultural na área de ensino em saúde da FPS. 
		É um serviço de informação científica dedicado ao gerenciamento da produção intelectual de uma instituição.<br>
		Contemplando a reunião, armazenamento, organização, preservação, recuperação e, principalmente, a disseminação da informação científica produzida pela instituição.<br>
		Neste viés, o repositório visa contemplar as produções bibliográficas e técnicas oriundos das Graduações e pós-graduações lato e stricto sensu, assim como as produções institucionais. 

		<br><br>
		
		<h4>1.	EQUIPE</h4>

		<ul>

			<li>Três bibliotecárias, que cuidam do tratamento (separação, descrição e indexação) dos materiais do repositório;</li>
			<li>um profissional de Tecnologia da Informação e Comunicação- TIC, para suporte técnico de implantação do software;</li>
			<li>um profissional de design visual e um profissional de marketing, para o desenvolvimento do desenho do layout e dinamicidade das funções da plataforma;</li>
			<li>dois pesquisadores responsáveis pela coordenação do projeto e acompanhamento do desenvolvimento técnico-metodológico do produto proposto.</li>

		</ul>
		
	</div>

<div class="row">
	<%@ include file="discovery/static-tagcloud-facet.jsp" %>
</div>
	
</div>
</dspace:layout>
