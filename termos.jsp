<%--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Home page JSP
  -
  - Attributes:
  -    communities - Community[] all communities in DSpace
  -    recent.submissions - RecetSubmissions
  --%>

<%@page import="org.dspace.core.Utils"%>
<%@page import="org.dspace.content.Bitstream"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="java.io.File" %>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Locale"%>
<%@ page import="javax.servlet.jsp.jstl.core.*" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.app.webui.components.RecentSubmissions" %>
<%@ page import="org.dspace.content.Community" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.NewsManager" %>
<%@ page import="org.dspace.browse.ItemCounter" %>
<%@ page import="org.dspace.content.Metadatum" %>
<%@ page import="org.dspace.content.Item" %>

<%
    Community[] communities = (Community[]) request.getAttribute("communities");

    Locale sessionLocale = UIUtil.getSessionLocale(request);
    Config.set(request.getSession(), Config.FMT_LOCALE, sessionLocale);
    String topNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-top.html"));
    String sideNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-side.html"));

    boolean feedEnabled = ConfigurationManager.getBooleanProperty("webui.feed.enable");
    String feedData = "NONE";
    if (feedEnabled)
    {
        feedData = "ALL:" + ConfigurationManager.getProperty("webui.feed.formats");
    }
    
    ItemCounter ic = new ItemCounter(UIUtil.obtainContext(request));

    RecentSubmissions submissions = (RecentSubmissions) request.getAttribute("recent.submissions");
%>

<dspace:layout locbar="nolink" titlekey="jsp.home.title" feedData="<%= feedData %>">

	<!--div class="jumbotron">
        <%= topNews %>
	</div-->

<div class="container row">
	
	<div class="col-md-12">
	
		<h3>Termos de uso</h3>
		
		<p>
			O Repositório Institucional é mantido pela Biblioteca da <strong>Faculdade Pernambucana de Saúde</strong>.<br>
			Suas funções são hospedar e Preservar o conteúdo digital acadêmico da FPS, potencializar o impacto das pesquisas acadêmicas e técnicas desenvolvidas na área de educação e de ensino em saúde, dar visibilidade nacional e internacional à produção científica e cultural da instituição; estimular a mais ampla circulação do conhecimento, a fim de fortalecer o compromisso institucional com o livre acesso à informação científica em saúde; além de conferir transparência e incentivar a comunicação científica entre pesquisadores, educadores, acadêmicos, gestores, alunos de pós-graduação e toda a sociedade civil.<br>
			O uso do Repositório Institucional (RDI) da FPS e seu conteúdo está sujeito aos termos e condições aqui estabelecidos, além das leis aplicáveis. Ao utilizar o RDI bem como seus conteúdos, você aceita e concorda com estes “Termos de Uso”. Caso não concorde com qualquer um dos “Termos de Uso”, recomendamos não utilizar o repositório institucional do FPS.<br>

			<h3>Alterações nos Termos de Uso</h3>
			Os “Termos de Uso” poderão ser alterados a qualquer momento sem aviso prévio.<br>
			O uso do RDI- FPS implica concordância com os “Termos de Uso” atualizados. <br>
			É recomendada a leitura dos “Termos de Uso” regularmente para acompanhar suas atualizações.<br>

			<h3>Proteção dos Direitos Autorais</h3>
			O Repositório Institucional da FPS e os diferentes textos, imagens, gráficos, informações e outros conteúdos do repositório podem ser protegidos por direitos autorais ou outras formas de proteção intelectual. Assim, o acesso ao conteúdo bem como seu uso deve ser orientado a partir do estabelecido nestes “Termos de Uso”.<br>
			Qualquer utilização fora ou além do autorizado neste documento é de inteira e total responsabilidade dos usuários, que, por meio desta, isentam a Instituição de qualquer responsabilidade, assumindo para si todos os ônus e despesas que porventura vierem a ser efetuados em razão da violação destes “Termos de Uso” ou direitos sobre as obras contidas neste Repositório.<br>

			<h3>Política de Acesso</h3>
			O material depositado no RI estão sujeitos à Política de Acesso a Informação da FPS.<br>

			<h3>Finalidades dos Usos</h3>
			As obras intelectuais depositadas, em acesso aberto, no RDI- FPS poderão ser utilizadas gratuitamente por qualquer pessoa, física ou jurídica, para fins privados, pessoais, educacionais, de pesquisa, científicos, informativos, de arquivamento, preservação, difusão, divulgação, demonstração, disponibilização e outras finalidades não comerciais.<br>

			<h3>Usos autorizados</h3>
			Os usuários do RDI- FPS estão, por meio e nos limites destes “Termos de Uso”, autorizados a reproduzir, exibir, executar, declamar, recitar, expor, arquivar, inserir em bancos de dados, difundir, distribuir, divulgar, disponibilizar, traduzir, legendar, subtitular, incluir em novas obras ou coletâneas, ou qualquer forma de utilizar o material disponibilizado, desde que não haja finalidade comercial e sejam respeitados os direitos morais, dando-se sempre os devidos créditos aos autores originais.<br>

			<h3>Direitos Morais</h3>
			São reservados exclusivamente ao AUTOR os direitos morais sobre as obras de sua autoria e/ou titularidade, sendo os terceiros usuários responsáveis pela atribuição de autoria e manutenção da integridade da OBRA em qualquer utilização.<br>

			<h3>Direitos Reservados</h3>
			O usuário concorda expressamente em usar o RDI-FPS somente de acordo com os usos permitidos nestes Termos de Uso ou estabelecidos legalmente.<br>
			Todos os usos não expressamente autorizados são reservados aos titulares. São especialmente reservados aos autores e titulares todos os direitos morais e os usos comerciais.<br>
			Quando as OBRAS forem disponibilizadas em outros espaços virtuais deverão sempre citar a fonte original e utilizar os links para o endereço virtual do Acervo.<br>
			Em qualquer hipótese as OBRAS não poderão sofrer quaisquer alterações quanto à autoria, título e integridade do material originalmente depositado, ou ser utilizadas para a prática de atos ilícitos, ou para qualquer finalidade não expressamente prevista neste documento ou na legislação aplicável.<br>
			Toda e qualquer autorização para os usos não expressamente autorizados por estes “Termos de Uso” ou em desacordo com os usos livres legalmente estabelecidos deverão ser expressamente autorizadas pelo AUTOR ou TITULAR. Os interessados deverão entrar em contato com (tcc@fps.edu.br)<br>

			<h3>Isenção de Responsabilidade</h3>
			O RDI-FPS adota medidas necessárias para garantir a segurança da informação, entretanto, não é possível garantir com exatidão, confiabilidade e integridade que o repositório e/ou que qualquer um de seus conteúdos continuarão sempre disponíveis. Não há garantias também sobre o funcionamento do RDI- FPS sem erros ou interrupção, ou que o site ou seus servidores estão livres de vírus ou de outros materiais nocivos.<br>

			<h3>Limitações e exceções legais</h3>
			O RDI-FPS reconhece e respeita integralmente os direitos difusos e coletivos da sociedade de acesso ao conhecimento e produção científica e cultural estabelecidos.<br>
			Assim, nada nos presentes “Termos de Uso” destina-se a restringir, limitar, sobrepor, substituir ou de qualquer forma afastar ou reduzir o escopo das limitações e exceções legais aos direitos autorais.<br>

			<h3>Limitações e Responsabilidades</h3>
			A equipe responsável pela manutenção do RDI- FPS não aprova nenhum conteúdo depositado por terceiros, incluindo os autores da Faculdade Pernambucana de Saúde. Cabe exclusivamente ao depositante a responsabilidade de garantir a autoria e titularidade da obra, a confiabilidade da informação e os critérios de permissão.<br>

			<h3>Links para outros sites</h3>
			Os links de terceiros, disponíveis no RIDI-FPS, são fornecidos apenas como referência e para fins de conveniência para o usuário. Os conteúdos apresentados nestes links bem como o alcance e qualidade do acesso dos mesmos não são de responsabilidade do RDI-FPS.<br>

			<h3>Reclamações de direitos autorais</h3>
			O respeito os direitos autorais de terceiros. Caso identifique qualquer tipo de violação de direitos neste RI, favor comunicar à Coordenação do RDI- FPS.<br>

			<h3>Disposições Gerais</h3>
			Se qualquer disposição destes “Termos de Uso” for considerada inválida ou inexequível, essa disposição será inutilizada sem afetar a legitimidade e validade das disposições restantes. O uso do RDI-FPS, ou de qualquer um dos conteúdos disponibilizados por meio deste, configura a absoluta concordância por parte do(s) usuário(s) com os “Termos de Uso” vigentes.<br>
		
		</p>
		
	</div>

<div class="row">
	<%@ include file="discovery/static-tagcloud-facet.jsp" %>
</div>
	
</div>
</dspace:layout>
